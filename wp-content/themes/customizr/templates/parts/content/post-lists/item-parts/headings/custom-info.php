<div class="date-catagory">
  <div class="date"><?php $post_date = get_the_date( 'Y .m .d' ); echo $post_date; ?></div>
  <div class="cat">
    <?php
    $category = get_the_category();
    echo '<a href="#" class="category-name category-link" type="'.$category[0]->cat_ID.'"><p class="home_category_link">'.$category[0]->cat_name.'</p></a>';
     ?>
  </div>
</div>
