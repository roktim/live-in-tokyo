<?php
/**
 * The template for displaying the single post content
 *
 * In WP loop
 */
?>
  <article <?php echo czr_fn_get_the_singular_article_selectors() ?> <?php czr_fn_echo( 'element_attributes' ) ?>>
    <?php do_action( '__before_content' ) ?>
    <?php
    /* heading */
    czr_fn_render_template( 'content/singular/headings/regular_post_heading' );
    ?>
    <div class="post-entry tc-content-inner">
      <section class="post-content entry-content <?php czr_fn_echo( 'element_class' ) ?>" >
        <?php
        czr_post_format_part();
        the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>' , 'customizr' ) );
        ?>
        <footer class="post-footer fb_likes">
            <?php
              if (is_single()) {
                require_once('fb_like.php');
              }
               ?>
        </footer>
      </section><!-- .entry-content -->
    </div><!-- .post-entry -->
    <?php do_action( '__after_content' ) ?>
  </article>
  <!-- SHOW ALL CATEGORY NAME -->
<div class="all-category">
  <?php
  $categories = get_categories( array(
      'orderby' => 'name',
      'order'   => 'ASC'
  ) );
  foreach( $categories as $category ) {
      //$category->cat_ID;
      $category_link = sprintf(
          '<a class="category-name category-link" type="'.$category->cat_ID.'" href="%1$s">'.$category->name.'</a>',
          esc_url( get_category_link( $category->term_id ) ),
          esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
          esc_html( $category->name )
      );
      echo $category_link;
  }
  ?>
</div>
