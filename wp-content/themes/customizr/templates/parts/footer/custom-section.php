<section class="category_section">
  <div class="logo-item">
    <div class="item1">
      <a href="https://www.kabocha-b.jp/?utm_source=corporate&utm_medium=servicepage" target="_blank"><img src="<?php  echo get_template_directory_uri(); ?>/inc/assets/img/kabocha.png" alt=""></a>
    </div>
    <div class="item2">
      <a href="https://www.step-cloud.jp/?utm_source=corporate&utm_medium=servicepage" target="_blank"><img src="<?php  echo get_template_directory_uri(); ?>/inc/assets/img/step_cloud.png" alt=""></a>
    </div>
    <div class="item3">
      <a href="https://wh-tokyo.com/?utm_source=corporate&utm_medium=servicepage" target="_blank"><img src="<?php  echo get_template_directory_uri(); ?>/inc/assets/img/logo_Tokyo.png" alt=""></a>
    </div>
  </div>
  <div class="footer-nav">
    <ul>
      <li><a href="<?php echo get_site_url();?>/about">About </a></li>
      <li><a href="<?php echo get_site_url();?>/policy">Privacy Policy </a></li>
      <li><a href="https://www.smt-days.co.jp/">Smart Days Co.,Ltd.</a></li>
    </ul>
  </div>
</section>
