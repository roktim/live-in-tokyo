<?php
/**
 * The template for displaying the standard colophon
 *
 * @package Customizr
 * @since Customizr 3.5.0
 */
?>
<div id="colophon" class="colophon colophon__row row flex-row justify-content-between" <?php czr_fn_echo('element_attributes') ?>>
  <div class="col-md-12 col-sm-auto text-center">
    <?php require_once('custom-section.php'); ?>
    <?php if ( czr_fn_is_registered_or_possible( 'footer_credits' ) ) czr_fn_render_template( 'footer/footer_credits' ) ?>
  </div>
</div>
